import React, { Component } from 'react';
import AddMovie from './components/AddMovie/AddMovie';
import Movie from './components/Movie/Movie';
import './App.css';

class App extends Component {

  state = {
    movies : [],
    movie: '',
  };


  addMovie = () => {

    const copyArray = [...this.state.movies];
    const newMovie = {movie: this.state.movie};
    copyArray.push(newMovie);
    this.setState({movies: copyArray})

  };

  changeHandler = event => {
    this.setState({
      movie: event.target.value
    })
  };

  deleteMovie = (index) => {
    const copyTexts = [...this.state.movies];

    copyTexts.splice(index, 1);

    this.setState({movies: copyTexts});

  };

  TryChangeMovie = (event, index) => {
    const movies = this.state.movies;
    const movie = movies[index];
    movie.movie = event.target.value;
    this.setState({
      movies
    })
  };

  render() {
    return (
        <div className="App">

          <AddMovie onChange={(event) => this.changeHandler(event)} addMovie={() => this.addMovie()}/>

          {this.state.movies.map((movie, index) => {
            return (
                <Movie
                    key={index}
                  deleteMovie={this.deleteMovie}
                  onChange={this.TryChangeMovie}
                  value={movie.movie}
                    id={index}
                />
            )
          })}
        </div>
    );
  }
}

export default App;