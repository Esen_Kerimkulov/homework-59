import React, {Component} from 'react';
import './Movie.css';

class Movie extends Component {

    shouldComponentUpdate(nextProps) {
        return nextProps.value !== this.props.value
    }



    render() {
        console.log(this.props.value);
        return (
            <div className="block-task">
                <p>
                    <input className="inputt" type="text" value={this.props.value} onChange={(event) => this.props.onChange(event, this.props.id)}/>
                </p>
                <button className="delete-btn" onClick={() => this.props.deleteMovie(this.props.id)}>{'\u2718'}</button>

            </div>
        )
    }

}

export default Movie;