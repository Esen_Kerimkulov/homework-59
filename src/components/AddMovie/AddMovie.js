import React from 'react';
import './AddMovie.css';

const AddMovie = props => {

    return (
        <div className="main-block">

            <input className="input" type="text"
                   placeholder="Add the movie you want to watch"
                   value={props.movie}
                   onChange={props.onChange}
            />

            <button className="btn"
                    onClick={props.addMovie}
            >
                Add
            </button>

            <h2>To watch list:</h2>

        </div>
    );
};

export default AddMovie;